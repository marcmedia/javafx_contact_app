package de.marcmedia.contact;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;



/**
 *
 * @author Marc
 *
 */
public class ContactController implements Initializable {
	
	
	private final int NAME_LENGTH = 2;
	private final int EMAIL_LENGTH = 4;
	private final int PHONE_LENGTH = 6;

    @FXML
    private HBox leftHBox;

    @FXML
    private TableView<Contact> contactTableView;
    
    @FXML
    private TextField idTextfield;

    @FXML
    private TextField firstNameTextfield;

    @FXML
    private TextField lastNameTextfield;

    @FXML
    private TextField emailTextfield;

    @FXML
    private TextField phoneTextfield;

    @FXML
    private Button storeButton;

    @FXML
    private Button cancelButton;

    @FXML
    private Button showContacts;
    
    @FXML
    private Button buttonUpdate;
    
    @FXML
    private Button buttonDelete;
    
    @FXML
    private TableColumn<Contact, Integer> idColumn;

    @FXML
    private TableColumn<Contact, String> firstNameColumn;

    @FXML
    private TableColumn<Contact, String> lastNameColumn;

    @FXML
    private TableColumn<Contact, String> emailColumn;

    @FXML
    private TableColumn<Contact, String> phoneColumn;

    private DatabaseHandler dbHandler = new DatabaseHandler();

    //private Connection connection;

    private ObservableList<Contact> allContacts;
    


    /**
     * 
     * @param event
     */
    @FXML
    void cancelButtonPressed(final ActionEvent event) {
    	clearFields();
    }


    /**
     * 
     * @param event
     */
    @FXML
    void showContactsButtonPressed(final ActionEvent event) {
    	loadContacts();
    }
    
    
    @FXML
    void handleRowSelect(MouseEvent event) {
    	if (event.getClickCount() == 2) {
    		Contact contact = contactTableView.getSelectionModel().getSelectedItem();
    		loadContactToTextfields(contact);
    		buttonUpdate.setDisable(false);
    	}
    }
    
    
    
    


    /**
     *
     */
    public void loadContacts() {
		allContacts = dbHandler.loadContacts();

		idColumn.setCellValueFactory(new PropertyValueFactory<Contact, Integer>("idContact"));
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("firstName"));
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("lastName"));
		emailColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("email"));
		phoneColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("phone"));

		contactTableView.setItems(allContacts);
    }


    /**
     *
     * @param event
     */
    @FXML
    void onKeyReleasedProperty(final KeyEvent event) {
    	String firstName = firstNameTextfield.getText();
    	String lastName = lastNameTextfield.getText();
    	String email = emailTextfield.getText();
    	String phone = phoneTextfield.getText();

    	if (firstName.isEmpty() && lastName.isEmpty() && email.isEmpty() && phone.isEmpty()) {
    		cancelButton.setDisable(true);
    	} else {
    		cancelButton.setDisable(false);
    	}

    	if (firstName.trim().isEmpty() || firstName.length() < NAME_LENGTH
    				|| lastName.trim().isEmpty() || lastName.length() < NAME_LENGTH
    				|| email.trim().isEmpty() || email.length() < EMAIL_LENGTH
    				|| phone.trim().isEmpty() || phone.length() < PHONE_LENGTH) {
    		storeButton.setDisable(true);
    	} else {
    		storeButton.setDisable(false);
    	}
    }


    /**
     *
     * @param event
     */
    @FXML
    void storeButtonPressed(final ActionEvent event) {
    	String firstName = firstNameTextfield.getText();
    	String lastName = lastNameTextfield.getText();
    	String email = emailTextfield.getText();
    	String phone = phoneTextfield.getText();

    	dbHandler.insertContact(firstName, lastName, email, phone);

    	loadContacts();
    	
    	clearFields();
    }
    
    
    /**
     * 
     * @param event
     */
    @FXML
    void buttonUpdateTapped(ActionEvent event) {
    	int idContact = Integer.valueOf(idTextfield.getText());
    	String firstName = firstNameTextfield.getText();
    	String lastName = lastNameTextfield.getText();
    	String email = emailTextfield.getText();
    	String phone = phoneTextfield.getText();
    	
    	if (!idTextfield.getText().isEmpty()) {
        	dbHandler.updateContact(idContact, firstName, lastName, email, phone);
        	
        	loadContacts();
        	
        	clearFields();
    	} else {
    		createAlertWindow(AlertType.INFORMATION, "Information", "There is no contact selected to update!", "")
    							.showAndWait();
    	}
    }
    
    
    /**
     * 
     * @param event
     */
    @FXML
    void deleteButtonTapped(ActionEvent event) {
    	if (!idTextfield.getText().isEmpty()) {
    		Alert alert = createAlertWindow(
    				AlertType.CONFIRMATION, 
    				"Please Confirm", 
    				"You are about to delete the Contact with the id " + idTextfield.getText(), 
    				"Do you agree with that?");
    		Optional<ButtonType> result = alert.showAndWait();
    		
    		if (result.get() == ButtonType.OK) {
        		dbHandler.deleteContact(Integer.valueOf(idTextfield.getText()));
        		loadContacts();
    		} 
    		else {
    			return;
    		}
    	} 
    	else {
    		createAlertWindow(AlertType.INFORMATION, "Information", "Nothing selected to delete!", "").show();
    	}
    }
    
    
    /**
     * 
     * @param alertType
     * @param title
     * @param headerText
     * @param contentText
     * @return
     */
    private Alert createAlertWindow(AlertType alertType, String title, 
    		String headerText, String contentText) {
    	Alert alert = new Alert(alertType);
    	alert.setTitle(title);
    	alert.setHeaderText(headerText);
    	alert.setContentText(contentText);
    	
    	return alert;
    }

    
    /**
     * 
     * @param contact
     */
    private void loadContactToTextfields(Contact contact) {
    	idTextfield.setText(String.valueOf(contact.getIdContact()));
    	firstNameTextfield.setText(contact.getFirstName());
    	lastNameTextfield.setText(contact.getLastName());
    	emailTextfield.setText(contact.getEmail());
    	phoneTextfield.setText(contact.getPhone());
    }
    
    
    /**
     * 
     */
    private void clearFields() {
    	idTextfield.clear();
    	firstNameTextfield.clear();
    	lastNameTextfield.clear();
    	emailTextfield.clear();
    	phoneTextfield.clear();
    	
    	storeButton.setDisable(true);
    	
    	cancelButton.setDisable(true);
    }


    /**
     *
     */
	@Override
	public void initialize(final URL location, final ResourceBundle resources) {
		storeButton.setDisable(true);
		
		cancelButton.setDisable(true);
		
		idTextfield.setEditable(false);
		
		buttonUpdate.setDisable(true);

		loadContacts();
	}

}
