package de.marcmedia.contact;

/**
 * 
 * @author Marc
 *
 */
public class Contact {

	private int idContact;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;

	
	/**
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phone
	 */
	public Contact(final int idContact, final String firstName, final String lastName, 
			final String email, final String phone) {
		this.idContact = idContact;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
	}
	
	
//	public int getidContact() {
//		return this.idContact;
//	}
//	
//	
//	public void setidContact(int idContact) {
//		this.idContact = idContact;
//	}
	
	public int getIdContact() {
		return idContact;
	}


	public void setIdContact(int idContact) {
		this.idContact = idContact;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String getFirstName() {
		return this.firstName;
	}





	/**
	 * 
	 * @return
	 */
	public String getLastName() {
		return this.lastName;
	}


	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return this.email;
	}


	/**
	 * 
	 * @return
	 */
	public String getPhone() {
		return this.phone;
	}


}
