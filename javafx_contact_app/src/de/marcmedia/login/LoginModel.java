package de.marcmedia.login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.marcmedia.contact.DatabaseHandler;



/**
 * 
 * @author Marc
 *
 */
public class LoginModel {
	
	
	DatabaseHandler dbHandler = new DatabaseHandler();
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @param connection
	 * @return
	 * @throws Exception
	 */
	public boolean isLoggedIn(String username, String password) throws Exception {
		
		ResultSet resultSet = null;
		boolean isLoggedIn = false;
		
		Connection connection = dbHandler.getConnection();
		
		String sql = "SELECT * FROM Login WHERE username = ? AND password = ?";
		
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setString(1, username);
			preparedStatement.setString(2, password);
			
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				isLoggedIn = true;
				
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isLoggedIn;
	}

}
