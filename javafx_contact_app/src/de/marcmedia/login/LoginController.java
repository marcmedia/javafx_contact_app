package de.marcmedia.login;

import java.net.URL;
import java.util.ResourceBundle;

import de.marcmedia.contact.DatabaseHandler;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * 
 * @author Marc
 *
 */
public class LoginController implements Initializable {

    @FXML
    private Label heading;

    @FXML
    private TextField usernameTextfield;
    
    @FXML
    private PasswordField passwordTextfield;
    
    @FXML
    private Label errorLabelUsername;
    
    @FXML
    private Label wrongEntryLabel;
    
    @FXML
    private Label errorLabelPassword;

    @FXML
    private Button actionButton;
    
    private LoginModel loginModel = new LoginModel();
    
	private final int USERNAME_LENGTH = 3;
	
	private final int PASSWORD_LENGTH = 4;
    
    
    /**
     * 
     * @param event
     */
    @FXML
    void loginButtonClicked(ActionEvent event) {   	
    	String username = usernameTextfield.getText();
    	String password = passwordTextfield.getText();
    	
    	try {
    		
    		if(loginModel.isLoggedIn(username, password)) {
    			successLogin(event);
    		} else {
				wrongEntryLabel.setVisible(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
    }
    
    
    /**
     * 
     * @param event
     * @throws Exception
     */
    private void successLogin(Event event) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("../view/Contacts.fxml"));
		Scene scene = new Scene(root);
			
		Stage primaryStage = (Stage)((Node) event.getSource()).getScene().getWindow();
		primaryStage.setScene(scene);
    }


    /**
     * 
     */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		actionButton.setDisable(true);
		errorLabelUsername.setVisible(false);
		errorLabelPassword.setVisible(false);
		wrongEntryLabel.setVisible(false);
		
	}
	
	
	/**
	 * 
	 * @param event
	 */
    @FXML
    void keyReleasedProperty(KeyEvent event) {
    	String usernameText = usernameTextfield.getText();
    	String passwordText = passwordTextfield.getText();
    	TextField textField = (TextField) event.getSource();
    	String sourcePromptText = textField.getPromptText();		
    	
    	if (sourcePromptText.equals("Username")) {
    		if (!isValidUsername(usernameText)) {
    			errorLabelUsername.setVisible(true);
    		} 
    		else {
    			errorLabelUsername.setVisible(false);
    		}
    	} else if (sourcePromptText.equals("Password")) {
    		if (!isValidPassword(passwordText)) {
    			errorLabelPassword.setVisible(true);
    		} 
    		else {
    			errorLabelPassword.setVisible(false);
    		}
    	}
    	boolean isDisabled = !isValidUsername(usernameText) || !isValidPassword(passwordText);
    	
    	actionButton.setDisable(isDisabled);
    }
    
    
    /**
     * 
     * @param username
     * @return
     */
    private boolean isValidUsername(String username) {
    	if (username.isEmpty() || username.trim().isEmpty() || username.length() < USERNAME_LENGTH) {
    		return false;
    	}
    	return true;
    }
    
    
    /**
     * 
     * @param password
     * @return
     */
    private boolean isValidPassword(String password) {
    	if (password.isEmpty() || password.trim().isEmpty() || password.length() < PASSWORD_LENGTH) {
    		return false;
    	}
    	return true;
    }
    
    
    /**
     * 
     * @param input
     * @param event
     */
    private void showErrorMessageOnInputfield(String input, KeyEvent event) {
    	TextField txtfField = (TextField) event.getSource();
    	String source = txtfField.getPromptText();
    	
    	if (source.equals("Username")) {
    		if (!isValidUsername(input)) {
    			errorLabelUsername.setVisible(true);
    		} 
    		else {
    			errorLabelUsername.setVisible(false);
    		}
    	} 
    	else {
    		if (!isValidPassword(input)) {
    			errorLabelPassword.setVisible(true);
    		} 
    		else {
    			errorLabelPassword.setVisible(false);
    		}
    	}
    }
}