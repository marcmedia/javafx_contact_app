package de.marcmedia.contact;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;



/**
 * 
 * @author Marc
 *
 */
public class DatabaseHandler {
	
	public static final String DB_NAME = "Contacts.db";
	public static final String DB_PATH = "src/";
	public static final String CONNECTION_STRING = "jdbc:sqlite:" + DB_PATH + DB_NAME;
	
	private final String TABLE_CONTACT = "Contact";
	
	// INSERT INTO Contact (idContact, firstName, lastName, email, phone) values (?, ?, ?, ?) 
	private final String INSERT_CONTACT_INTO_DATABASE = 
			"INSERT INTO " + TABLE_CONTACT 
			+ " (firstName, lastName, email, phone)"
			+ " VALUES (?, ?, ?, ?)";
	
	private final String QUERY_ALL_CONTACTS_FROM_DATABASE = "SELECT * FROM Contact";
	
	private final String UPDATE_CONTACT = "UPDATE " + TABLE_CONTACT + " SET "
					+ "firstName = ?, "
					+ "lastname = ?, "
					+ "email = ?, " 
					+ "phone = ? "
					+ "WHERE idContact = ";
	
	private final String DELETE_CONTACT = "DELETE FROM " + TABLE_CONTACT 
						+ " WHERE idContact = ";
	
	private Connection connection;
	
	
	/**
	 * 
	 */
	public DatabaseHandler() {
		createConnection();
	}
	
	/**
	 * 
	 * @param idContact
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phone
	 * @return
	 */
	public boolean insertContact(
			String firstName, 
			String lastName, 
			String email, 
			String phone) {
		boolean insertionSuccessful = false;
		
		try (Connection connection = DriverManager.getConnection(CONNECTION_STRING);
				PreparedStatement preparedStatement = 
						connection.prepareStatement(INSERT_CONTACT_INTO_DATABASE);) {
			
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			preparedStatement.setString(3, email);
			preparedStatement.setString(4, phone);	
			
			preparedStatement.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return insertionSuccessful;
	}
	
	
	/**
	 * 
	 * @param connection
	 * @param contactList
	 * @return
	 */
	public ObservableList<Contact> loadContacts() {
		ObservableList<Contact>contactList = FXCollections.observableArrayList();
		ResultSet resultSet;
		try (Connection connection = DriverManager.getConnection(CONNECTION_STRING);
				PreparedStatement selectContacts = 
						connection.prepareStatement(QUERY_ALL_CONTACTS_FROM_DATABASE);) {
			resultSet = selectContacts.executeQuery();
			
			while (resultSet.next()) {
				int idContact = resultSet.getInt(1);
				String firstName = resultSet.getString(2);
				String lastName = resultSet.getString(3);
				String email = resultSet.getString(4);
				String phone = resultSet.getString(5);
				
				contactList.add(new Contact(idContact, firstName, lastName, email, phone));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return contactList;
	}
	
	
	/**
	 * 
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phone
	 */
	public void updateContact(
			final int id, 
			final String firstName, 
			final String lastName, 
			final String email, 
			final String phone) {
		try (Connection connection = DriverManager.getConnection(CONNECTION_STRING);
				PreparedStatement updateContact = connection.prepareStatement(UPDATE_CONTACT + id)) {
			updateContact.setString(1, firstName);
			updateContact.setString(2, lastName);
			updateContact.setString(3, email);
			updateContact.setString(4, phone);
			
			updateContact.executeUpdate();
			
			System.out.println(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void deleteContact(final int id) {
		try (Connection connection = DriverManager.getConnection(CONNECTION_STRING);
				PreparedStatement deleteContact = connection.prepareStatement(DELETE_CONTACT + id)) {
			deleteContact.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Connection getConnection() {
		return this.connection;
	}
	
	
	/**
	 * Private Helper Class that creates the connection to the Database.
	 */
	private void createConnection() {
		try {
			this.connection = DriverManager.getConnection(CONNECTION_STRING);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
